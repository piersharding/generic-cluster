---
- name: Load default variables
  include_vars:
    file: ../vars/default.yml

- name: resolve platform specific vars
  include_vars: '{{ node_vars }}'
  with_first_found:
    - files:
        - '{{ ansible_distribution }}-{{ ansible_distribution_release }}.yml'
        - '{{ ansible_distribution }}.yml'
        - '{{ ansible_os_family }}.yml'
      errors: ignore
      paths:
        - '{{ role_path }}/../../vars'
        - '{{ role_path }}/vars'
  loop_control:
    loop_var: node_vars

- name: Set hostnames
  hostname: name="{{inventory_hostname}}"
  when: not inventory_hostname == "localhost"
  become: true

- name: update /etc/hosts
  lineinfile:  dest=/etc/hosts regexp='{{ ansible_default_ipv4.address }} .*{{inventory_hostname}}'
              line="{{ ansible_default_ipv4.address }} {{inventory_hostname}}"
              state=present
  when: not inventory_hostname == "localhost"
  register: updatehosts
  become: true

- name: update /etc/hosts others
  lineinfile:  dest=/etc/hosts regexp='.*{{hostvars[item].inventory_hostname}}.*'
              line="{{hostvars[item].ansible_default_ipv4.address}} {{hostvars[item].inventory_hostname}}"
              state=present
  with_items: '{{groups["cluster"]}}'
  when: not item == "localhost"
  register: updatehosts
  become: true


# mount up all the file-systems
- name: Check if /var/lib/docker file-system is already mounted
  shell: /bin/mountpoint -q "/var/lib/docker"
  ignore_errors: yes
  register: docker_fs_checked
  changed_when: "docker_fs_checked.rc != 0"

- name: First time mount of docker
  block:
    - name: Format docker_libs
      filesystem:
        fstype: xfs
        dev: /dev/vdb

    - name: "Get UUID for docker_libs partition"
      command: blkid -s UUID -o value /dev/vdb
      register: docker_disk_blkid
      changed_when: False

    - name: Creates directory /var/lib/docker
      file:
        path: /var/lib/docker
        state: directory

    - name: mount docker_libs
      mount:
        name: /var/lib/docker
        src: 'UUID={{ docker_disk_blkid.stdout }}'
        opts: defaults,noatime
        dump: "0"
        passno: "0"
        fstype: xfs
        state: mounted
  when: not docker_fs_checked.rc == 0

- name: Check if data file-system is already mounted
  shell: /bin/mountpoint -q "{{ data_filesystem }}"
  ignore_errors: yes
  register: fs_checked
  changed_when: "fs_checked.rc != 0"

- name: First time mount of data
  block:
    - name: Format data
      filesystem:
        fstype: xfs
        dev: /dev/vdc

    - name: "Get UUID for data partition"
      command: blkid -s UUID -o value /dev/vdc
      register: data_disk_blkid
      changed_when: False

    - name: Creates directory for data
      file:
        path: "{{ data_filesystem }}"
        state: directory

    - name: mount data
      mount:
        name: "{{ data_filesystem }}"
        src: 'UUID={{ data_disk_blkid.stdout }}'
        opts: defaults,noatime
        dump: "0"
        passno: "0"
        fstype: xfs
        state: mounted
  when: not fs_checked.rc == 0 and create_data_vol
# create_data_vol is passed to the heat template conditional for creating and
# attaching the data volume

- name: Ensure required packages are installed (apt)
  apt:
    name: "{{ packages|default([]) }}"
    update_cache: yes
    state: present
  when: ansible_os_family == "Debian"

- name: Ensure required packages are installed (dnf)
  dnf:
    name: "{{ packages|default([]) }}"
    state: present
  when: ansible_os_family == "RedHat"

- name: Upgrade all packages (apt)
  apt:
    upgrade: dist
    autoremove: yes
  when: ansible_os_family == "Debian"

- name: upgrade all packages (dnf)
  dnf:
    name: "*"
    state: latest
  when: ansible_os_family == "RedHat"

- name: Make sure the NTP service is stopped
  service: name={{ ntp_service }} state=stopped

- name: Force NTP sync
  command: "{{ ntp_sync }}"

- name: Start the NTP service
  service: name={{ ntp_service }} state=started enabled=yes

# - name: Set ulimits limits
#   lineinfile: dest=/etc/security/limits.conf
#               insertbefore="^# End of file"
#               state=present
#               line="{{ item }}"
#   with_items:
#     - "* soft nofile 32768"
#     - "* hard nofile 32768"
#     - "* soft cpu unlimited"
#     - "* soft core unlimited"
#     - "* soft data unlimited"
#     - "* soft memlock unlimited"
#     - "* soft stack unlimited"
#     - "* soft fsize unlimited"
#     - "* soft msgqueue unlimited"
#     - "* soft as unlimited"
#     - "* hard as unlimited"
#     - "* soft memlock unlimited"
#     - "* hard cpu unlimited"
#     - "* hard core unlimited"
#     - "* hard data unlimited"
#     - "* hard memlock unlimited"
#     - "* hard stack unlimited"
#     - "* hard fsize unlimited"
#     - "* hard msgqueue unlimited"
#     - "* hard memlock unlimited"
#     - "* soft nproc 65536"
#     - "* hard nproc 65536"

- name: Set swappiness to 1
  sysctl: name=vm.swappiness value=1 state=present ignoreerrors=yes

- name: Disable Transparent Huge Pages in Grub 2
  lineinfile: dest=/etc/default/grub
              state=present
              line='GRUB_CMDLINE_LINUX=$GRUB_CMDLINE_LINUX" transparent_hugepage=never"'
  # when: ansible_distribution_major_version|int > 6
  notify: run update-grub

- meta: flush_handlers

- name: Disable Transparent Huge Pages until reboot
  shell: echo never > /sys/kernel/mm/transparent_hugepage/enabled && echo never > /sys/kernel/mm/transparent_hugepage/defrag
  ignore_errors: true

