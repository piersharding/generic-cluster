---
heat_template_version: 2016-10-14

description: >
  Heat stack containing a group of groups of Nova instances.
  This is the top-level resource of a Cluster-as-a-Service deployment.

parameters:
  cluster_prefix:
    type: string
    label: Instance name prefix
    default: "cluster"
  cluster_groups:
    type: json
    label: List of dicts defining each group of nodes within the cluster
  cluster_keypair:
    type: string
    label: SSH key pair name for admin access to the cluster nodes
  cluster_az:
    type: string
    label: Availability zone
    default: nova
  docker_vol_size:
    type: number
    label: Docker Volume size - GB
    default: 20
  data_vol_size:
    type: number
    label: Data Volume size - GB
    default: 50
  create_data_vol:
    type: boolean
    label: Create data vol - true/false?
    default: false
  cluster_net:
    type: json
    label: Network names and subnets to which the nodes should be attached
  secondary_net:
    type: json
    label: Network names and subnets to which the nodes should be attached

resources:
  secgroup:
    type: OS::Neutron::SecurityGroup
    properties:
      name: 
        list_join: 
        - '-'
        - [ { get_param: cluster_prefix },
            'master-group' ]      
      description: Master Security Group
      rules:
        - protocol: icmp
        - remote_ip_prefix: 192.168.100.0/24
          protocol: tcp
          # port_range_min: 0
          # port_range_max: 65535
        - remote_ip_prefix: 192.168.100.0/24
          protocol: udp
          # port_range_min: 0
          # port_range_max: 65535

  cluster_group:
    type: OS::Heat::ResourceGroup
    properties:
      count:
        yaql:
          data: 
            cluster_groups: { get_param: cluster_groups }
          expression: $.data.cluster_groups.len()
      resource_def:
        type: cluster-group.yaml
        properties:
          cluster_prefix: { get_param: cluster_prefix }
          cluster_keypair: { get_param: cluster_keypair }
          cluster_az: { get_param: cluster_az }
          docker_vol_size: { get_param: docker_vol_size }
          data_vol_size: { get_param: data_vol_size }
          create_data_vol: { get_param: create_data_vol }
          cluster_groups: { get_param: cluster_groups }
          cluster_net: { get_param: cluster_net }
          secondary_net: { get_param: secondary_net }
          secgroup_id: { get_resource: secgroup }
          group_idx: "%index%"

outputs:
  cluster_group:
    description: Instance data for cluster node groups
    value: { get_attr: [cluster_group, group_data] }
