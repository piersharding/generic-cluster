---

# This name is used for the Heat stack and as a prefix for the
# cluster node hostnames.
cluster_name: test_generic

# This parameter should be set to the name of an RSA keypair you have
# uplaoded to OpenStack.
cluster_keypair: rsa_keypair

image: "Ubuntu-18.04-x86_64"

# /var/lib/docker volume size - GB
docker_vol_size: 25

# data_vol (/dev/vdc) size - GB
data_vol_size: 50
# data_vol mount point
data_filesystem: /data
# activate data vol
# create_data_vol is passed to the heat template conditional for creating and
# attaching the data volume
create_data_vol: true

# Site-specific network configuration.
cluster_net:
  - { net: "int_net", subnet: "subnet1", "floatingip_net_id": "1fea99f2-36ed-481d-b495-6e50453f5956" }

secondary_net:
  - { net: "xxx-bdn", subnet: "xxx-bdn" }

# Multi-node application topology.  In this case we have a Dask
# scheduler node (which also offers login services) and a number
# of compute nodes, which mount the same filesystems but are not
# intended to be interactive.
cluster_groups:
  - "{{ genericnode_master }}"
  - "{{ genericnode_worker }}"

genericnode_master:
  name: "master"
  flavor: "m1.medium"
  image: "{{ image }}"
  num_nodes: 1

genericnode_worker:
  name: "worker"
  flavor: "m1.medium"
  image: "{{ image }}"
  num_nodes: 3

# Node group assignments for cluster roles.
# These group assignments are appended to the cluster inventory file.
# The names of these roles are cross-referenced to groups referred to
# in playbooks in the ansible/ directory.
cluster_roles:
  - name: "nodes"
    groups: "{{ cluster_groups }}"
  # - name: "login"
  #   groups: [ "{{ genericnode_master }}", "{{ genericnode_worker }}" ]

cluster_group_vars:
  cluster:
    ansible_user: ubuntu 
    ansible_python_interpreter: python3
