KEY_PAIR ?= your-key
IMAGE_ID ?= Ubuntu-18.04-x86_64
FLAVOR ?= m1.small
PRIVATE_VARS ?= private_vars.yml
INVENTORY_FILE ?= ./inventory_cluster

.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

ifndef KEY_PAIR
    $(error KEY_PAIR has not been supplied eg: make <action> KEY_PAIR=your-key)
endif

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "KEY_PAIR=$(KEY_PAIR)"
	@echo "IMAGE_ID=$(IMAGE_ID)"
	@echo "FLAVOR=$(FLAVOR)"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"

all: build_all ## call build_all

build: build_all ## call build_all

clean_node:  ## destroy out nodes
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ansible-playbook -e @playbooks/group_vars/generic \
	                 -e @$(PRIVATE_VARS) \
					 -i playbooks/inventory \
					 playbooks/remove-cluster-infra.yml

clean_hosts: clean_node ## destroy hosts (alias for nodes)

clean_all: clean_hosts  ## destroy nodes and security group/s

clean: clean_all  ## call clean_all

build_node:  ## build the nodes (VMs)
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ansible-playbook -e @playbooks/group_vars/generic \
	                 -e @$(PRIVATE_VARS) \
					 -i playbooks/inventory playbooks/cluster-infra.yml \
	--extra-vars="image=$(IMAGE_ID) cluster_keypair=$(KEY_PAIR) flavor=$(FLAVOR) inventory_file=$(INVENTORY_FILE)"

check:  ## check that docker is running
	cd playbooks  && ansible -i ../$(INVENTORY_FILE) cluster \
	                         -m shell -a 'docker ps -a; df'

build_hosts: build_node ## build hosts (only)

build_docker:  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ansible-playbook  -e @playbooks/group_vars/generic \
	                  -e @$(PRIVATE_VARS) \
					  -i $(INVENTORY_FILE) playbooks/docker.yml \
	--extra-vars="image=$(IMAGE_ID) cluster_keypair=$(KEY_PAIR) flavor=$(FLAVOR)"

build_all: build_hosts build_docker  ## build hosts and then apply docker

restart:  ## Restart hosts
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ansible-playbook  -e @playbooks/group_vars/generic \
	                  -e @$(PRIVATE_VARS) \
					  -i $(INVENTORY_FILE) playbooks/restart-hosts.yml \
	--extra-vars="image=$(IMAGE_ID) cluster_keypair=$(KEY_PAIR) flavor=$(FLAVOR)"


help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
