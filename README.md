# generic-cluster

Example template for handling dynamic inventory with Ansible to build a generic cluster.


## Summary

This repo contains an example of building a set of generic nodes on OpenStack driven by Ansible and Heat Templates.  It will build a cluster of master and worker nodes within their own Security Group, and then format with a basic Docker install.  Inspired by https://github.com/stackhpc/ansible-role-cluster-infra .

`/var/lib/docker` is placed in a separate volume, and by default a data volume is also created (see `playbooks/group_vars/generic` and `playbooks/roles/stackhpc.cluster-infra/files/cluster-instance.yaml` for details).

The basic process to use is as follows:

source your `*-openrc.sh` file

Populate your personal `PrivateRules.mak` with something like:
```
KEY_PAIR = piers-engage
FLAVOR = m1.medium
```

Look at the `Makefile` vars and modify to taste (also can be added to `PrivateRules.mak`).

eg:
```
IMAGE_ID
```

Review `playbooks/group_vars/generic` and change `cluster_groups:`, `genericnode_master:`, and `genericnode_worker:` to reflect the specifications of nodes to be created.  Change `cluster_name` to a value that will be used to prefix all resources created eg: volumes, security groups, instances etc.

These values can be overridden locally by creating a `private_vars.yml` file in the root of the project - eg:
```
cluster_name: piers_test_generic
# supress the creation of the data volume - see playbooks/group_vars/generic for more details
create_data_vol: false
```

To build use `make build`, to tear everything down again use `make clean`.

## Prerequisites

Ensure that your `ssh` can find client certificate related to the `KEY_PAIR` used for the stack build eg: `${HOME}/.ssh/config` has been configured with:
```
...
IdentityFile /home/your-home/.ssh/yourkeypair-private-key.pem
...
```

Also, you need to make sure that the OpenStack account used has `stack_owner`, as well as `member` (sometimes `_member`) roles.
